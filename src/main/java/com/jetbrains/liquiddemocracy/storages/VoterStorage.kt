package com.jetbrains.liquiddemocracy.storages

import com.jetbrains.liquiddemocracy.models.Delegate
import com.jetbrains.liquiddemocracy.models.Voter

interface VoterStorage {
    fun getVoters(delegate: Delegate): Collection<Voter>

    fun chooseDelegate(voter: Voter, delegate: Delegate)
}