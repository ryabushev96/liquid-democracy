package com.jetbrains.liquiddemocracy.storages

import com.jetbrains.liquiddemocracy.models.Delegate

interface DelegateStorage {
    fun addDelegate(userId: String, tag: String): Delegate

    fun deleteDelegate(delegate: Delegate)

    fun getDelegate(userId: String): Delegate
}