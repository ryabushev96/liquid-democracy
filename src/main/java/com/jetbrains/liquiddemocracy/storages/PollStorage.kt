package com.jetbrains.liquiddemocracy.storages

import com.jetbrains.liquiddemocracy.models.Answer
import com.jetbrains.liquiddemocracy.models.Poll
import com.jetbrains.liquiddemocracy.models.Voter

interface PollStorage {
    fun createPoll(questionId: String): Poll

    fun vote(voter: Voter, poll: Poll)

    fun getPoll(answer: Answer): Poll

}