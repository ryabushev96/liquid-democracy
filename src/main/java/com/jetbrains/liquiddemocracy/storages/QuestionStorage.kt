package com.jetbrains.liquiddemocracy.storages

import com.jetbrains.liquiddemocracy.models.Answer
import com.jetbrains.liquiddemocracy.models.Question

interface QuestionStorage {
    fun create(question: String, answers: List<Answer>): Question

    fun getById(questionId: String): Question
}