package com.jetbrains.liquiddemocracy.models

interface Answer {
    val questionId: String
    val userId: String
    val answerId: String
}
