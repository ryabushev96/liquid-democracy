package com.jetbrains.liquiddemocracy.models

interface Voter {
    val userId: String
    val delegateId: String
}