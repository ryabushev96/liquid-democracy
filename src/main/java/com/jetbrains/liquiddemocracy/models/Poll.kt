package com.jetbrains.liquiddemocracy.models

interface Poll {
    val pollId: String
    val tag: String
}
