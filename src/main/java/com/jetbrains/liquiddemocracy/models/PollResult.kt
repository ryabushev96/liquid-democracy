package com.jetbrains.liquiddemocracy.models

interface PollResult {
    val pollId: String
    val results: Map<String, Int>
}
