package com.jetbrains.liquiddemocracy.services.delegate

import com.jetbrains.liquiddemocracy.models.Voter

interface DelegateService {
    fun createDelegate(userId: String, tag: String)

    fun deleteDelegate(delegateId: String)

    fun isDelegate(userId: String): Boolean

    fun getVoters(delegateId: String): Collection<Voter>

    fun chooseDelegate(voterId: String, delegateId: String)
}