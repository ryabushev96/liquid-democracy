package com.jetbrains.liquiddemocracy.services.poll

import com.jetbrains.liquiddemocracy.models.Poll
import com.jetbrains.liquiddemocracy.models.PollResult

interface PollService {
    fun createPoll(question: String, answers: List<String>, tag: String): Poll

    fun vote(questionId: String, userId: String)

    fun getResults(questionId: String): PollResult

    fun getTag(): String
}